# ERea

[Dossier contenant les fichiers audios](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/)

## Index des ressources audios


* [Exemple 1](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_1.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_1.wav
```

* [Exemple 2](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_2.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_2.wav
```

* [Exemple 3](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_3.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_3.wav
```

* [Exemple 4](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_4.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_4.wav
```

* [Exemple 5](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_5.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_5.wav
```

* [Exemple 6](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_6.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_6.wav
```

* [Exemple 7](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_7.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_7.wav
```

* [Exemple 8](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_8.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_8.wav
```

* [Exemple 10](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_10.wav)

```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_10.wav
```

* [Exemple 15](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_15.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_15.wav
```

* [Exemple 16](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_16.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_16.wav
```

* [Exemple 17](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_17.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_17.wav
```

* [Exemple 18](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_18.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_18.wav
```

* [Exemple 19](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_19.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_19.wav
```

* [Exemple 20](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_20.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_20.wav
```

* [Exemple 21](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_21.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_21.wav
```

* [Exemple 22](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_22.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_22.wav
```

* [Exemple 23](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_23.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_23.wav
```

* [Exemple 24](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_24.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_24.wav
```

* [Exemple 25](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_25.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_25.wav
```

* [Exemple 26](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_26.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_26.wav
```

* [Exemple 27](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_27.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_27.wav
```

* [Exemple 28](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_28.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_28.wav
```

* [Exemple 29](https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_29.wav)
```
lien : https://gitlab.huma-num.fr/mshs-poitiers/forellis/erea/blob/master/datas/Exemple_29.wav
```

